# Interactiv4 Library Dev Tools

Description
-----------
This metapackage contains common dependencies to develop libraries.


Versioning
----------
This package does not follow semver versioning.

It has branch-aliased versions, related with the php versions it supports:

PHP 7.0 : 7.0.x-dev, dev-php-70

PHP 7.1 : 7.1.x-dev, dev-php-71

PHP 7.2 : 7.2.x-dev, dev-php-72

PHP 7.3 : 7.3.x-dev, dev-php-73

(...)

PHP {php_version} : {php_version}.x-dev, dev-php-{php_version}


Compatibility
-------------
- PHP ^7.3


Installation Instructions
-------------------------
It can be installed manually using composer, by typing the following command:

`composer require --dev interactiv4/library-dev-tools:@dev --update-with-all-dependencies`


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/library-dev-tools/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/library-dev-tools/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2019 Interactiv4 S.L.